using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardAxie : MonoBehaviour
{
    [SerializeField] private GameObject _grayParent, _whiteParent;
    [SerializeField] private List<GameObject> _listParent;
    [SerializeField] private Transform _boarAxie;
    private void Start()
    {
        //InitBoard();
    }
    public void InitBoard()
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                if (i%2==0 && j%2==0)
                {
                    //GameObject ob = new GameObject();
                    var ob = Instantiate<GameObject>(_grayParent, _boarAxie);
                    ob.transform.position = new Vector3(i*10, j*10, 0);
                    _listParent.Add(ob);
                }    
                else
                {
                    var ob = Instantiate<GameObject>(_whiteParent, _boarAxie);
                    ob.transform.position = new Vector3(i * 10, j * 10, 0);
                    _listParent.Add(ob);
                }
                    
            }
        }
    }
}
