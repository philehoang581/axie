﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private Vector3 _origin;
    private Vector3 _difference;
    private Vector3 _resetCamera;
    private float _minBoundY = 0;
    private float _maxBoundY = 100;
    private float _minBoundX = 0;
    private float _maxBoundX = 100;

    private bool drag = false;

    private void Start()
    {
        _resetCamera = Camera.main.transform.position;
    }

    public void UpdateSizeMap(float input)
    {
        Camera.main.orthographicSize += input;
        if (Camera.main.orthographicSize < 50) Camera.main.orthographicSize = 50;
        if (Camera.main.orthographicSize > 100) Camera.main.orthographicSize = 100;

    }
    private void Isometric()
    {
         
        Quaternion target = Quaternion.Euler(75, 75, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 10f);
    }
    private void ResetIsometric()
    {
        Quaternion target = Quaternion.Euler(0, 0, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) Isometric();
        if(Input.GetKeyUp(KeyCode.Tab)) ResetIsometric();


        if (Input.GetMouseButton(0))
        {
            _difference = (Camera.main.ScreenToWorldPoint(Input.mousePosition)) - Camera.main.transform.position;
            if (drag == false)
            {
                drag = true;
                _origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            } 
        }
        else
        {
            drag = false;
        } 
        if (drag)
        { 
            Vector3 tempPos = _origin - _difference * 0.5f;
            Camera.main.transform.position = new Vector3(Mathf.Clamp(tempPos.x, _minBoundX, _maxBoundX), Mathf.Clamp(tempPos.y, _minBoundY, _maxBoundY),-100);
             
        }

        if (Input.GetMouseButton(1))
        {
            drag = false;
           // Camera.main.transform.position = _resetCamera;
            _difference = Vector3.zero;
            _origin = Vector3.zero; 
            //_difference = _origin;
        }
        
    }
}