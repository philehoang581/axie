using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _jumpAudio, _attackAudio;

    public void PlayAudio(AudioType audioType)
    {
        if(audioType== AudioType.ATTACK)
            _audioSource.PlayOneShot(_attackAudio); 
        else
            _audioSource.PlayOneShot(_jumpAudio);
    }
}
public enum AudioType
{
    JUMP,
    ATTACK 
}
