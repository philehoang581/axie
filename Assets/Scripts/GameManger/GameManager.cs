using System.Collections;
using System.Collections.Generic;
using AxieCore.AxieMixer;
using AxieMixer.Unity;
using Newtonsoft.Json.Linq;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;
using System;
public class GameManager : MonoBehaviour
{
    //Attack team ID: 4191804
    //genesStr: "0x20000000000001000281a000810c0000000100102080830a000100141061020a000100101880c4060001000c0880c50c000100101820c4060001001008804402"
    //Defense team ID: 2724598
    //genesStr: "0x18000000000001000240a050c210000000010008286045040001000c08814302000100042021430c0001000c2861430a0001000c3061830c0001000c08404302"
    //Board 12x12 

    private AxieGenerate _axieAttack = new AxieGenerate();
    private AxieGenerate _axieDefense = new AxieGenerate();
    [Header("UI")]
    [SerializeField] private UIManager _uIManger;
    [SerializeField] private AxieInfo _axieInfoPanel; 
    [SerializeField] private AudioManager _audioManager; 
    [SerializeField] private GameObject _parentAxieAttack, _parentAxieDefense;
    [SerializeField] private List<AxieEntity> _listAxieAttack, _listAxieDefense;
    private List<Axie> listAdjacent = new List<Axie>();
    private float _speedFeature = 2; 
    private bool _isPauseGame;
     
    private Axie2dBuilder builder => Mixer.Builder;
    /// <summary>
    /// Cell Empty: 0
    /// Attack:     1
    /// Defense:    2
    /// </summary>
    private int[,] _matrixAxie = {
        { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,},
        { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
        { 1, 1, 0, 2, 2, 2, 2, 0, 1, 1,},
        { 1, 1, 0, 2, 2, 2, 2, 0, 1, 1,},
        { 1, 1, 0, 2, 2, 2, 2, 0, 1, 1,},
        { 1, 1, 0, 2, 2, 2, 2, 0, 1, 1,},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
        { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,},
        { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,},
    };
    private Axie[,] _matrixAxieEntity = new Axie[10,10];
     
    private void Start()
    {
        Mixer.Init();
        GenerateAxie();
        LoadAxie(); 
        StartCoroutine(AutoHit());

        _uIManger.SetupUI(
            (speed)=>
            {
                _speedFeature = speed;
            },  
            (pause)=>
            {
                PauseGame(pause);
            }
            );
    } 
    private void GenerateAxie()
    {
        _axieDefense.axieId = "4191804";
        _axieDefense.genesStr = "0x20000000000001000281a000810c0000000100102080830a000100141061020a000100101880c4060001000c0880c50c000100101820c4060001001008804402";
        _axieDefense.isGraphic = false;
        
        _axieAttack.axieId = "2724598";
        _axieAttack.genesStr = "0x18000000000001000240a050c210000000010008286045040001000c08814302000100042021430c0001000c2861430a0001000c3061830c0001000c08404302";
        _axieAttack.isGraphic = false;
    } 
    private void LoadAxie()
    {
        int countAttack = 0;
        int countDefense = 0;
        for (int x = 0; x < 10; x++)
        {
            for (int y = 0; y < 10; y++)
            {
                if (_matrixAxie[x, y] == 1) //Team Attack
                { 
                    AxieEntity axieEntity = new AxieEntity();
                    axieEntity.HP = axieEntity.CurrentHP = 32;
                    axieEntity.IndexX = x;
                    axieEntity.IndexY = y;
                    axieEntity.TypeAxie = TypeAxie.ATTACK;
                    _listAxieAttack.Add(axieEntity); 
                    _parentAxieAttack = CreateAxie(_parentAxieAttack, _axieAttack.axieId, _axieAttack.genesStr, axieEntity, $"ATTACK {x}-{y}"); 
                    _parentAxieAttack.transform.GetChild(countAttack).position = new Vector3(x * 10, y * 10, 0); 
                    _matrixAxieEntity[x, y] = _parentAxieAttack.transform.GetChild(countAttack).GetComponent<Axie>();
                    countAttack++;
                }
                else if (_matrixAxie[x, y] == 2) //Team Defense
                { 
                    AxieEntity axieEntity = new AxieEntity();
                    axieEntity.HP = axieEntity.CurrentHP = 16;
                    axieEntity.IndexX = x;
                    axieEntity.IndexY = y;
                    axieEntity.TypeAxie = TypeAxie.DEFENSE;
                    _listAxieDefense.Add(axieEntity);  
                    _parentAxieDefense = CreateAxie(_parentAxieDefense, _axieDefense.axieId, _axieDefense.genesStr, axieEntity, $"DEFENSE {x}-{y}");
                    _parentAxieDefense.transform.GetChild(countDefense).position = new Vector3(x * 10, y * 10, 0); 
                    _matrixAxieEntity[x, y] = _parentAxieDefense.transform.GetChild(countDefense).GetComponent<Axie>();
                    countDefense++;
                }
                else
                {
                    _matrixAxieEntity[x, y] = null;
                }
            }
        }
    }
     
    private IEnumerator AutoHit()
    {
        while (_parentAxieAttack.transform.childCount>0 && _parentAxieDefense.transform.childCount>0)
        {
            yield return new WaitForSeconds(_speedFeature);
            CheckNenemy();  
        }
    }
    private void PauseGame(bool ispause)
    {
        _isPauseGame = ispause; 
        if (ispause)
            StopAllCoroutines();
        else
            StartCoroutine(AutoHit());
    }
    
    public void CheckNenemy()
    {
        bool isMove = true; 
        for (int i = 0; i < _parentAxieAttack.transform.childCount; i++)
        {  
            int x = _parentAxieAttack.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexX;
            int y = _parentAxieAttack.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexY; 
            if (_matrixAxieEntity[x, y] == null) return;
            listAdjacent = FindAdjacent(_matrixAxieEntity[x, y]);
             
            if (CheckCellType(x, y) == TypeAxie.ATTACK)
            {
                isMove = true;
                for (int j = 0; j < listAdjacent.Count; j++)
                {
                    if (isMove == true && listAdjacent[j] != null && listAdjacent[j].AxieEntity.TypeAxie == TypeAxie.DEFENSE)
                    {
                        isMove = false;
                        Attack(_matrixAxieEntity[x, y], listAdjacent[j]);
                    }
                } 
                if (isMove) 
                {
                    Vector2Int direction = GetDirection(new Vector2Int(x,y));
                    if (direction != new Vector2Int(-1, -1))
                    {
                        Vector2Int targetPoint = GetPointMove(new Vector2Int(x, y), direction);
                        if (targetPoint != new Vector2Int(-1, -1)) Move(_matrixAxieEntity[x,y], targetPoint);
                        //Debug.LogError($"Move: {x}-{y} ===> {targetPoint}");
                    } 
                }
            }
        }

        for (int i = 0; i < _parentAxieDefense.transform.childCount; i++)
        {
            int x = _parentAxieDefense.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexX;
            int y = _parentAxieDefense.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexY;
            if (_matrixAxieEntity[x, y] == null) return;
            List<Axie> listAdjacent = FindAdjacent(_matrixAxieEntity[x, y]);

            if(_matrixAxieEntity[x, y] != null)
            {
                for (int j = 0; j < listAdjacent.Count; j++)
                {
                    if (listAdjacent[j] != null && listAdjacent[j].AxieEntity.TypeAxie == TypeAxie.ATTACK) Attack(_matrixAxieEntity[x, y], listAdjacent[j]);
                }
            } 
        } 
    }
     
    [SerializeField] private List<Vector2Int> _listPath = new List<Vector2Int>(); 
    public Vector2Int GetDirection(Vector2Int startPoint)
    {
        _listPath.Clear();
        for (int i = 0; i < _parentAxieDefense.transform.childCount; i++)
        {
            int x = _parentAxieDefense.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexX;
            int y = _parentAxieDefense.transform.GetChild(i).GetComponent<Axie>().AxieEntity.IndexY;
            Vector2Int indexTarget = new Vector2Int(x,y); 
            _listPath.Add((startPoint - indexTarget));  
        }

        int index = 0;
        int countStep = 100;
        for (int i = 0; i < _listPath.Count; i++)
        {
            int step = Math.Abs(_listPath[i].x) + Math.Abs(_listPath[i].y);
            if (countStep > step)
            {
                countStep = step;
                index = i;
            }
        } 
        if (_listPath.Count > 0)
            return _listPath[index];
        else
            return new Vector2Int(-1, -1);
    }
    private Vector2Int GetPointMove(Vector2Int startPoint, Vector2Int direction)
    { 
        int x = direction.x;
        int y = direction.y;

        if (x == 0)
        {
            if (y > 0)
            {
                startPoint.y = Mathf.Clamp(startPoint.y - 1, 0, 9);
                if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
            }
            else
            {
                startPoint.y = Mathf.Clamp(startPoint.y + 1, 0, 9);
                if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
            } 
        }

        if (y == 0)
        {
            if (x > 0)
            {
                startPoint.x = Mathf.Clamp(startPoint.x - 1, 0, 9);
                if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
            }
            else
            { 
                startPoint.x = Mathf.Clamp(startPoint.x + 1, 0, 9);
                
                if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
            }
        }

        if (x > 0 && y > 0)
        {
            startPoint.x = Mathf.Clamp(startPoint.x - 1, 0, 9);
            if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
        }

        if (x < 0 && y < 0)
        {
            startPoint.x = Mathf.Clamp(startPoint.x + 1, 0, 9);
            if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
        }

        if (x > 0 && y < 0)
        {
            startPoint.y = Mathf.Clamp(startPoint.y + 1, 0, 9);
            if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
        }
        if (x < 0 && y > 0)
        {
            startPoint.y = Mathf.Clamp(startPoint.y - 1, 0, 9);
            if (CheckCellType(startPoint.x, startPoint.y) == TypeAxie.NULL) return startPoint;
        } 
        return new Vector2Int(-1, -1);
    }
     
    private List<Axie> FindAdjacent(Axie axie)
    { 
        List<Axie> _listAdjacent = new List<Axie>();
        int x = axie.AxieEntity.IndexX;
        int y = axie.AxieEntity.IndexY;

        if (x > 0 && y > 0 && x < 9 && y < 9)
        {
            if (CheckCellType(x + 1, y) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x + 1, y]);
            if (CheckCellType(x - 1, y) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x - 1, y]);
            if (CheckCellType(x, y + 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x, y + 1]);
            if (CheckCellType(x, y - 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x, y - 1]);

            if (CheckCellType(x - 1, y - 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x - 1, y - 1]);
            if (CheckCellType(x + 1, y + 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x + 1, y + 1]);
            if (CheckCellType(x - 1, y + 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x - 1, y + 1]);
            if (CheckCellType(x + 1, y - 1) != TypeAxie.NULL) _listAdjacent.Add(_matrixAxieEntity[x + 1, y - 1]);
        }
        return _listAdjacent;
    }   
    private TypeAxie CheckCellType(int x, int y)
    { 
        if (_matrixAxieEntity[x, y] == null)
            return TypeAxie.NULL;
        else
            return _matrixAxieEntity[x, y].AxieEntity.TypeAxie;
    }

    private void Move(Axie axieFrom, Vector2Int vector2Int)
    {
        if (axieFrom == null)
        {
            return;
        }
        _audioManager.PlayAudio(AudioType.JUMP);
        int x = axieFrom.AxieEntity.IndexX;
        int y = axieFrom.AxieEntity.IndexY;
        
        StartCoroutine(SmoothMove(axieFrom.gameObject, new Vector3(vector2Int.x * 10, vector2Int.y * 10, 0)));
        axieFrom.AxieEntity.IndexX = vector2Int.x;
        axieFrom.AxieEntity.IndexY = vector2Int.y;

        Axie tempFrom = axieFrom;  
        Axie tempTo = _matrixAxieEntity[vector2Int.x, vector2Int.y]; 
        axieFrom = tempTo;

        _matrixAxieEntity[vector2Int.x, vector2Int.y] = tempFrom;
        _matrixAxieEntity[x, y] = null; 
    }

    private IEnumerator SmoothMove(GameObject ob, Vector3 targetPosition)
    {
        float timeMove = 0;
        while (timeMove < 1)
        {
            yield return new WaitForEndOfFrame();
            if(ob!=null) ob.transform.position = Vector3.Lerp(ob.transform.position, targetPosition, timeMove);
            timeMove += Time.deltaTime;
        }
    }

    private void Attack(Axie axieAttack, Axie axieIsAttacked)
    {
        if (axieAttack == null || axieIsAttacked == null) return;
        _audioManager.PlayAudio(AudioType.ATTACK);
        float damage = 0;

        float attacker_number = UnityEngine.Random.Range(0,3);
        float target_number = UnityEngine.Random.Range(0,3);
        axieAttack.AxieEntity.ActtackDamage = attacker_number;
        axieIsAttacked.AxieEntity.ActtackDamage = target_number;


        if ((3 + attacker_number - target_number) % 3 == 0) damage = 4;
        if ((3 + attacker_number - target_number) % 3 == 1) damage = 5;
        if ((3 + attacker_number - target_number) % 3 == 2) damage = 3;
         
        _matrixAxieEntity[axieIsAttacked.AxieEntity.IndexX, axieIsAttacked.AxieEntity.IndexY].OnHitDamge(damage);
    }
    private void ShowAxieInfo(AxieEntity axieEntity)
    {
        _axieInfoPanel.ShowInfo(axieEntity);
    } 
    private void DestroyAxieDie(GameObject go)
    {
        Destroy(go);
    }

    #region INIT AXIE
    private GameObject CreateAxie(GameObject go, string axieId, string genesStr, AxieEntity axieEntity, string nameAxie)
    {
        float scale = 0.01f;
        var builderResult = builder.BuildSpineFromGene(axieId, genesStr, scale); 
        go.transform.localPosition = new Vector3(0f, -0.24f, 0f);

        SkeletonAnimation runtimeSkeletonAnimation = SkeletonAnimation.NewSkeletonAnimationGameObject(builderResult.skeletonDataAsset, false, nameAxie);
        runtimeSkeletonAnimation.gameObject.layer = LayerMask.NameToLayer("Player");
        runtimeSkeletonAnimation.transform.SetParent(go.transform, false);
        runtimeSkeletonAnimation.transform.localScale = Vector3.one; 
        runtimeSkeletonAnimation.gameObject.AddComponent<AutoBlendAnimController>(); 
        runtimeSkeletonAnimation.gameObject.AddComponent<BoxCollider2D>(); 

        runtimeSkeletonAnimation.gameObject.AddComponent<Axie>(); 
        var temp = runtimeSkeletonAnimation.gameObject.GetComponent<Axie>();
        runtimeSkeletonAnimation.gameObject.GetComponent<Axie>().Init(axieEntity, axieEntity => ShowAxieInfo(axieEntity), go => DestroyAxieDie(go)); 
        Spine.Unity.Examples.SpineGauge bloodBar = Resources.Load<Spine.Unity.Examples.SpineGauge>("BloodBbar");
        Spine.Unity.Examples.SpineGauge ob = Instantiate(bloodBar); 
        ob.gameObject.transform.SetParent(runtimeSkeletonAnimation.gameObject.transform, true);
        ob.transform.localScale = new Vector3(3,3); 
        runtimeSkeletonAnimation.state.SetAnimation(0, "action/idle/normal", true);

        if (builderResult.adultCombo.ContainsKey("body") &&
            builderResult.adultCombo["body"].Contains("mystic") &&
            builderResult.adultCombo.TryGetValue("body-class", out var bodyClass) &&
            builderResult.adultCombo.TryGetValue("body-id", out var bodyId))
        {
            runtimeSkeletonAnimation.gameObject.AddComponent<MysticIdController>().Init(bodyClass, bodyId);
        }
        //runtimeSkeletonAnimation.skeleton.FindSlot("shadow").Attachment = null;
        var temp1 = runtimeSkeletonAnimation.skeleton.FindSlot("shadow").Attachment; 
        return go;
    }
    #endregion 
}
