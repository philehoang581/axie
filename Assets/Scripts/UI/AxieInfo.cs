using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AxieInfo : MonoBehaviour
{
    [SerializeField] private Text _hpTxt, _damageTxt, _totalDamageTxt;
    public void ShowInfo(AxieEntity axieEntity)
    {
        _hpTxt.text = $"HP: {axieEntity.CurrentHP}/{axieEntity.HP}";
        _damageTxt.text = $"Damage: {axieEntity.ActtackDamage}";
        _totalDamageTxt.text = $"Damage: {axieEntity.TotalDamage}";
    }
}
