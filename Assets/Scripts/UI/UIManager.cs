using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class UIManager : MonoBehaviour
{
    private Action<int> _speedAction;
    private Action<bool> _pauseAction;
    [SerializeField] private Button _pauseBtn; 
    [SerializeField] private Sprite _spritePause, _spritePlay; 

    [SerializeField] private CameraMove _cameraControll;
    private bool _isPause = true;

    public void SetupUI(Action<int> action, Action<bool> pause)
    {
        _speedAction = action;
        _pauseAction = pause;
    }
    public void X2Speed()
    {
        if (_speedAction != null) _speedAction.Invoke(1);
    }
    public void Pause()
    {
        if (_pauseAction != null) _pauseAction.Invoke(_isPause);
        if (_isPause)
        {
            _isPause = false;
           // _pauseBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "CONTINUE";
            _pauseBtn.gameObject.GetComponent<Image>().sprite = _spritePlay;
        }
        else
        {
            _isPause = true;
            //_pauseBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "PAUSE";
            _pauseBtn.gameObject.GetComponent<Image>().sprite = _spritePause;
        } 
    }

    public void ZoomOut()
    {
        _cameraControll.UpdateSizeMap(-5);
    }
    public void ZoomIn()
    {
        _cameraControll.UpdateSizeMap(5);
    } 
}
