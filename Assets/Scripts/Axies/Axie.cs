using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine.Unity;

public class Axie : MonoBehaviour
{
    [SerializeField] private AxieEntity _axieEntity;
    [SerializeField] private float _attackDamage; 
    private Action<GameObject> _dieAction;
    private Action<AxieEntity> _infoAction;  
    public AxieEntity AxieEntity
    {
        get
        {
            return _axieEntity;
        }
    }

    public void Init(AxieEntity axieEntity, Action<AxieEntity> actionInfo,  Action<GameObject> die)
    {
        _axieEntity = axieEntity;
        _infoAction = actionInfo;
        _dieAction = die; 
    }
    public void OnHitDamge(float hitDamage)
    {
        _axieEntity.CurrentHP -= hitDamage;
        if (_axieEntity.CurrentHP <= 0)
        {
            _dieAction.Invoke(this.gameObject);
        }
        this.gameObject.transform.GetChild(0).GetComponent<Spine.Unity.Examples.SpineGauge>().fillPercent = (float)_axieEntity.CurrentHP / (float)_axieEntity.HP;
        this.gameObject.transform.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "attack/ranged/cast-high", false);
    }
    private void OnMouseDown()
    {
        if (_infoAction != null) _infoAction.Invoke(_axieEntity); 
    }
}
