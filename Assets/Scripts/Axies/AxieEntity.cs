using System;
using UnityEngine;
using System.Collections.Generic;
[Serializable]
public class AxieEntity
{
    public float HP;
    public float CurrentHP; 
    public float ActtackDamage; 
    public float TotalDamage;
    public int IndexX;
    public int IndexY;
    public TypeAxie TypeAxie;
}
public enum TypeAxie
{ 
    ATTACK,
    DEFENSE, 
    NULL
}
[Serializable]
public class AxieGenerate
{
    public string axieId;
    public string genesStr;
    public bool isGraphic; 
}

public class ListPath
{
    public List<Vector2Int> ListPathTop = new List<Vector2Int>();
    public List<Vector2Int> ListPathBottom = new List<Vector2Int>();
    public List<Vector2Int> ListPathRight = new List<Vector2Int>();
    public List<Vector2Int> ListPathLeft = new List<Vector2Int>();
}

